import sys
import time
from grid import scan_grid, multi_line_input, input_grid
from solver import solve


if __name__ == '__main__':
    mode = "X"
    while mode[0] not in ("F", "M", "I"):
        mode = input("Quel mode d'entrée voulez-vous utiliser?\n"
                     "F - Grille dans le fichier d'entrée (in.txt)\n"
                     "M - Grille multiligne (mode manuel, ligne par ligne)\n"
                     "I - Ile par ile (coordonnées puis valeur)\n").upper()
    if mode[0] == "F":
        with open("in.txt", "r") as file:
            g = scan_grid(multi_line_input(file))
    elif mode[0] == "M":
        print("Entrez la grille avec un espace là où il n'y a pas d'ile, et leur valeur de ponts max si il y en a une"
              "\nTerminez votre entrée par EOF")
        g = scan_grid(multi_line_input())
    else:
        g = input_grid()
    print("Voici la grille entrée :")
    print(g)

    start_time = time.time()
    try:
        if solve(g) is True:
            print("Une solution à été trouvée avec succès :")
        else:
            print("Aucune solution trouvée ...")
        print(g)
    except Exception as e:
        print("L'exception suivante à eu lieu pendant l'éxecution du programme, "
              "êtes vous sûrs que votre grille est valide?",
              file=sys.stderr)
        print(f"{e.__class__.__name__}: {e}", file=sys.stderr)
    print(f"Execution terminée en {time.time() - start_time}s")


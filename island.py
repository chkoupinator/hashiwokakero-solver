class Island:
    __slots__ = ("x", "y", "val", "max_val", "neighbors")

    def __init__(self, x: int, y: int, val: int):
        self.x = x
        self.y = y
        self.val = self.max_val = val
        self.neighbors: [Island] = []

    def __repr__(self):
        return f"Island({self.x}, {self.y}, {self.max_val})"

    @property
    def position(self):
        return self.x, self.y

    def incr(self):
        self.val += 1
        if self.val > self.max_val:
            raise Exception(f"Ile à la position {self.position} a essayé d'incrementer plus que son maximum")

    def decr(self):
        self.val -= 1
        if self.val < 0:
            raise Exception(f"Ile à la position {self.position} a essayé de décrémenter à moins de 0")


from typing import Union, List

from grid import Grid, Bridge, Island


def compute_forced(g: Grid) -> [Bridge]:
    added_bridges: [Bridge] = []
    for isl in g.islands_list:
        if isl.val == 0:
            continue
        neighbors_capacity = sum(nbr.val for nbr in isl.neighbors)

        if isl.max_val == 1:

            # compute 1s with a single neighbor
            if len(isl.neighbors) == 1:
                neighbor = isl.neighbors[0]
                br = Bridge.from_two_islands(neighbor, isl)
                if br not in g.bridges_queue:
                    g.add_bridge(br)
                    added_bridges.append(br)
            else:
                # compute 1s that have neighbors with 1 and only one acceptable neighbor
                acceptable = None
                for neighbor in isl.neighbors:  # type: Island

                    # acceptable neighbor
                    if neighbor.max_val != 1:

                        # there are two acceptable neighbors
                        if acceptable is not None:
                            acceptable = None
                            break
                        acceptable = neighbor

                if acceptable is not None:
                    br = Bridge.from_two_islands(acceptable, isl)
                    g.add_bridge(br)
                    added_bridges.append(br)

        # compute other lengths
        elif isl.val == neighbors_capacity:

            for neighbor in isl.neighbors:
                while neighbor.val > 0:
                    br = Bridge.from_two_islands(neighbor, isl)
                    if g.bridges_queue.count(br) < 2:
                        g.add_bridge(br)
                        added_bridges.append(br)
                    else:
                        break

        else:

            if isl.max_val == len(isl.neighbors) * 2:
                for neighbor in isl.neighbors:
                    br = Bridge.from_two_islands(neighbor, isl)
                    count = g.bridges_queue.count(br)
                    if count == 0:
                        g.add_bridge(br)
                        added_bridges.append(br)
                        br = Bridge.from_two_islands(neighbor, isl)
                        g.add_bridge(br)                     # second one (double bridge)
                        added_bridges.append(br)
                    elif count == 1:
                        g.add_bridge(br)
                        added_bridges.append(br)

            if isl.max_val == len(isl.neighbors) * 2 - 1:
                for neighbor in isl.neighbors:
                    br = Bridge.from_two_islands(neighbor, isl)
                    count = g.bridges_queue.count(br)
                    if count == 0:
                        g.add_bridge(br)
                        added_bridges.append(br)
    return added_bridges

# translation vers procédure en français
# TrouverChoixForces(g: Grille (var pontsRajoutes: ListePonts):
# var ile: Ile, capaciteVoisins: Entier, voisin: Ile, pont: Pont, acceptable: Pont
# debut:
# pour ile dans g.listeIles:
# 	si ile.valeur == 0:
# 		continuer (vers iteration suivante)
#
# 	capaciteVoisins <- SommeValeursDesVoisins(ile)
#
# 	si ile.valeurMax == 1:
#
# 		si Taille(ile.voisins) == 1:
# 			voisin <- ile.voisins[0]
# 			pont <- NouveauPontEntreDeuxIles(ile, voisin)
# 			si EstDansListe(pont, g.PileDesPonts):
# 				RajouterPont(g, pont)
# 				AjouterListe(pontsRajoutes, pont)
#
# 		sinon:
# 			acceptable <- null
# 			pour voisin dans ile.voisins:
#
# 				si voisin.valeurMax != 1:
#
# 					si acceptable != null:
# 						acceptable <- null
# 						break (sortir du pour)
# 					acceptable <- voisin
#
# 			si acceptable != null:
# 				pont <- NouveauPontEntreDeuxIles(ile, acceptable)
# 				RajouterPont(g, pont)
# 				AjouterListe(pontsRajoutes, pont)
#
# 	sinon si ile.valeur == capaciteVoisins:
# 		pour voisin dans ile.voisins:
# 			tantque voisin.valeur > 0:
# 				pont = NouveauPontEntreDeuxIles(voisin, ile)
# 				si NbOccurences(g.PileDesPonts, pont) < 2:
# 					RajouterPont(g, pont)
# 					AjouterListe(pontsRajoutes, pont)
# 				sinon:
# 					sortir du tantque
#
# 	sinon:
# 		si ile.valeurMax == Taille(ile.voisins) * 2:
# 			pour voisin dans ile.voisins:
# 				pont <- NouveauPontEntreDeuxIles(ile, voisin)
#
# 				si NbOccurences(g.PileDesPonts, pont) == 0:
# 					RajouterPont(g, pont)
# 					AjouterListe(pontsRajoutes, pont)
# 					pont <- NouveauPontEntreDeuxIles(ile, voisin)
# 					RajouterPont(g, pont)
# 					AjouterListe(pontsRajoutes, pont)
#
# 				sinon si NbOccurences(g.PileDesPonts, pont) == 1:
# 					RajouterPont(g, pont)
# 					AjouterListe(pontsRajoutes, pont)
#
# 		sinon si ile.valeurMax == Taille(ile.voisins) * 2 - 1:
# 			pour voisin dans ile.voisins:
# 				pont <- NouveauPontEntreDeuxIles(ile, voisin)
#
# 				si NbOccurences(g.PileDesPonts, pont) == 0:
# 					RajouterPont(g, pont)
# 					AjouterListe(pontsRajoutes, pont)
#
# retourner pontsRajoutes


depth = 0


def solve(g: Grid) -> Union[bool, List[Bridge]]:
    # global depth
    # current_depth = depth
    # if current_depth in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]:
    #     print("iterating at depth", " " * current_depth, current_depth)
    # print(f"{' '*current_depth}queue len at entry : {len(g.bridges_queue)}")
    # depth += 1
    if g.solved:
        return True

    forced_picks: [Bridge] = []
    len_new_forced_picks = -1
    while len_new_forced_picks:
        new_forced = compute_forced(g)
        len_new_forced_picks = len(new_forced)
        forced_picks.extend(new_forced)

    if g.solved:
        return True

    remaining_possible_picks: [Bridge] = g.get_remaining_possible_bridges()
    nb_of_archipelagos_before_pick = len(g.last_archipelagos_state)
    for bridge in remaining_possible_picks:

        g.add_bridge(bridge)
        if len(g.last_archipelagos_state) > nb_of_archipelagos_before_pick:
            g.cancel_last_bridge(bridge)
        else:
            if solve(g):
                return True
            else:
                g.cancel_last_bridge(bridge)
                # print(f"{' ' * current_depth}queue len after parent cleaning : {len(g.bridges_queue)}")
    while len(forced_picks) > 0:
        g.cancel_last_bridge(forced_picks.pop())

    #  picks a bridge from all the possible bridges that haven't been added in
    #  the queue yet then compute forced bridges from that choice
    #  if the recursive rest of all attempts to solve with that pick result in fails, cancel the forced picks from
    #  from last to first then the pick itself, pick the next one
    # PS: j'ai écrit ça dans un to-do pour moi même mais ça explique bien ce qui se passe, hésitez pas à me contacter
    # si vous voulez une traduction
    # print(f"{' '* current_depth}queue len at exit : {len(g.bridges_queue)}")
    # depth -= 1

# Traduction en procédure :
# Resoudre(g: Grille):
# var choixForces: ListePonts, nouveauxChoixForces: ListePonts, nbNouveauxChoixForces : Entier,
# pont: Pont, pontsPossiblesRestants: ListePonts, nbArchipelsAvantChoix: Entier, resultat: ListePonts ou Booleen
#
# si EstSolution(g):
# 	retourner Vrai
#
# nbNouveauxChoixForces = -1
#
# tantque nouveauxChoixForces != 0:
# 	nouveauxChoixForces <- TrouverChoixForces(g)
# 	nbNouveauxChoixForces <- Taille(nouveauxChoixForces)
# 	RajouterDansListe(choixForces, nouveauxChoixForces)
#
# si EstSolution(g):
# 	retourner Vrai
#
# pontsPossiblesRestants <- TrouverPontsPossibles(g)
# nbArchipelsAvantChoix <- Taille(g.DernierElementDePileArchipels)
#
# pour pont dans pontsPossiblesRestants:
# 	RajouterPont(g, pont)
# 	si Taille(g.DernierElementDePileArchipels) > nbArchipelsAvantChoix:
# 		AnullerPont(g, pont)
# 	sinon:
# 		resultat = Resoudre(g)
# 		si resultat est Vrai:
# 			retourner Vrai
# 		sinon:
# 			tantque Taille(resultat) > 0:
# 				AnullerPont(g, RetirerFinDeListe(resultat))
# 			AnullerPont(g, pont)
#
# retourner choixForces


# ancienne solution qui utilise des Island plutot que des Bridge, je me suis arrêté en plein millieu d'un rework
# de cette solution car j'ai remarqué que ça n'avait pas de sens et qu'il serait plus judicieux d'utiliser une liste de
# Bridge plutot

# def solve(g: Grid):
#
#     def all_connected():
#         pass
#         # notTODO
#
#     def bridges_maxed():
#         for isl in g.islands_list:
#             if isl.val > 0:
#                 return False
#         return True
#
#     def recurs_solve():
#
#         def compute_forced():
#             pass
#             # notTODO find all forced choices (islands with neighbors that don't have a choice but take that neighbor)
#
#         # notTODO work on bridges instead of islands, should make things much much faster
#         # print(frontier)
#         if all_connected() and bridges_maxed():
#             return True
#         for start in frontier:                                             # type: Island
#             if start.val == 0:
#                 continue
#             # print(f"Looking for islands to connect, starting from {start}")
#             for next_isl in start.neighbors:                # type: Island
#                 # print(next_isl)
#                 if start.val > 0 and next_isl.val > 0:
#                     br = Bridge.from_two_islands(start, next_isl)
#                     if g.no_intersecting_bridge(br):
#                         # print(f"can bridge between {start} and {next_isl}")
#                         frontier.append(next_isl)
#                         g.add_bridge(br)
#                         if recurs_solve():
#                             return True
#                         else:
#                             frontier.pop()
#                             g.cancel_last_bridge()
#
#         return False
#
#     frontier.append(g.islands_list[0])
#     return recurs_solve()

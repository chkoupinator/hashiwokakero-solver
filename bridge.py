from __future__ import annotations
from typing import Union
from island import Island


class Bridge:
    __slots__ = ("x", "y", "is_vertical", "length")

    def __init__(self, x: int, y: int, vertical: bool, length: int):
        """
        Initialise le pont
        :param x: la position x de départ du pont (avec x minimale)
        :param y: la position y de départ du pont (avec y minimale)
        :param vertical: si le point d'arrivée et de départ ont le même x (pont droit sur x)
        :param length: la longueur du pont
        """
        self.x, self.y, self.length, self.is_vertical = x, y, length, vertical

    def __repr__(self):
        return f"Bridge({self.x}, {self.y}, {len(self)}, " \
               f"{'v' if self.is_vertical else 'h'})"

    @classmethod
    def from_coordinates(cls, pos1: (int, int), pos2: (int, int)):
        x1, y1 = pos1
        x2, y2 = pos2
        x, y = min(x1, x2), min(y1, y2)
        static_x = x1 == x2
        if not static_x and y1 != y2:
            raise Exception("Tentative de créer un pont entre deux points qui n'ont ni un X ni un Y commun")
        length = abs(x1 - x2) if not static_x else abs(y1 - y2)
        return cls(x, y, static_x, length)

    @classmethod
    def from_two_islands(cls, start: Island, end: Island) -> Bridge:
        return cls.from_coordinates((start.x, start.y), (end.x, end.y))
        # x, y = min(start.x, end.x), min(start.y, end.y)
        # static_x = end.x == start.x
        # length = abs(end.x - start.x) if static_x else abs(end.y - start.y)
        # return cls(x, y, static_x, length)

    @property
    def coordinates(self) -> ((int, int), (int, int)):
        x, y = self.x, self.y
        endx, endy = (x + len(self), y) if not self.is_vertical else (x, y + len(self))
        return (x, y), (endx, endy)

    def __len__(self):
        return self.length

    def intersects(self, x: int, y: int) -> bool:
        if x != self.x and y != self.y:
            return False        # optimisation mineure
        elif self.is_vertical:
            return self.y < y < self.y + len(self)
        else:
            return self.x < x < self.x + len(self)

    def would_intersect(self, other: Bridge) -> bool:
        if other.is_vertical == self.is_vertical:
            return False    # parallèles par nature si ils sont égaux

        xst, yst = (self, other) if self.is_vertical else (other, self)
        return xst.y < yst.y < xst.y + len(xst) and yst.x < xst.x < yst.x + len(yst)
        # en d'autres termes, si le x du pont avec un x fixe est entre les deux x de l'autre et
        # le y du pont avec un y fixe est entre les deux y de l'autre

    def __eq__(self, other: Union[Bridge, tuple]) -> bool:
        # retourne true si :
        #   other est un pont équivalent
        #   other est un tuple des deux iles reliées par ce pont
        #   other représente les coordonées reliées par le pont ((x1, y1), (x2, y2))
        def is_coords(t: tuple):
            return isinstance(t, tuple) and isinstance(t[0], int) and isinstance(t[1], int)

        if isinstance(other, Bridge):
            return self.x == other.x and self.y == other.y and self.is_vertical == other.is_vertical and \
                   len(self) == len(other)

        elif isinstance(other, tuple) and isinstance(other[0], Island) and isinstance(other[1], Island):
            br = Bridge.from_two_islands(other[0], other[1])
            return self.__eq__(br)

        elif isinstance(other, tuple) and is_coords(other[0]) and is_coords(other[1]):
            br = Bridge.from_coordinates(other[0], other[1])
            return self.__eq__(br)

        return False


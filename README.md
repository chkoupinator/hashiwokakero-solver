Ce solveur a été fait suite à un projet d'Algorithmique 3 qui nous a été donné; Voici une partie du rapport de projet en version Markdown

# I - Présentation du Jeu

Hashiwokakero est un jeu d’origine japonaise inventé par les créateurs du Sudoku dont le but est de connecter toutes les “îles” d’une grille de jeu par des “ponts”, selon des règles bien définies. Les îles sont représentées habituellement par un nombre à l'intérieur d’un cercle, un pont par une ligne entre deux îles. Il est à noter que dans mon programme, en raison des limitations de temps il n’y a qu’une représentation textuelle des grilles, et donc elles ne sont pas encerclées, mais cela sera expliqué plus tard.


L’objectif est donc de relier toutes les îles, sachant que:

- Une île doit impérativement avoir son nombre indiqué de ponts connectés.
- Il ne peut pas y avoir plus de deux ponts entre deux îles.
- Les ponts ne peuvent être qu’en ligne droite horizontale ou verticale, et entre deux
îles (qui ont donc au moins une coordonnée commune).
- Un pont ne peut que s'arrêter sur une île et non la survoler.
- Deux ponts différents ne peuvent se croiser.
- Si une solution d’une grille de Hashiwokakero existe, elle est unique.
- Chaque île doit être accessible à partir de chaque île de la grille.

# II - Notice d’utilisation
Comme mentionné plus haut, afin de gérer les entrées et les sorties, j’ai utilisé une interface texte simple (via terminal), avec trois méthodes principales pour soumettre une grille, comme expliqué au lancement du programme et un résultat affiché sous la même forme, en utilisant de nouveaux caractères introduits pour représenter les ponts.

## II.0 - Lancement du Programme
Étant donné que le programme est réalisé en Python, il est nécessaire d’avoir un interpréteur Python de version 3.6 ou supérieure (seule la 3.9 a été testée) et de lancer le script `main.py`
La commande à lancer dépend de votre système d’exploitation :
Sous Windows :
`py main.py`
Sous GNU/Linux ou MacOS :
`python3 main.py`
Le programme n’utilise aucune dépendance et n’utilise aucune bibliothèque non standard.
Il est à noter que si vous comptez utiliser le mode `F` expliqué dans la section suivante, il faudra bien penser à créer un fichier `in.txt` dans le répertoire principal, s’il n’existe pas déjà.

## II.1 - Gestion de l’entrée
Au lancement du programme, cette invite est initialement affichée afin de vous aider à choisir les options :
```
Quel mode d'entrée voulez-vous utiliser?
F - Grille dans le fichier d'entrée (in.txt)
M - Grille multiligne (mode manuel, ligne par ligne)
I - Île par île (coordonnées puis valeur)
```

- Dans le cas où vous entrez F, le fichier in.txt sera lu. Son contenu doit être sous la forme suivante, pour l’exemple de la grille ci-dessus : 
`in.txt`
```
2   3 3
 1 4 4

3  3 2
    1 2
2  4 2
EOF
```

- Dans le cas où vous entrez M, il vous sera demandé d’écrire la même chose que ce qu’il y a dans le fichier, ligne par ligne :
```
Entrez la grille avec un espace la ou il n'y a pas d'île, et leur valeur de ponts max si il y en a une
Terminez votre entrée par EOF
```
> À noter que EOF ici est bel et bien les trois caractères ‘E’, ‘O’ et ‘F’ (et non le caractère ASCII spécial End of File, End of Text ou End of Transmission).

- Dans le cas où vous entrez I, il vous sera demandé de donner la taille de la grille puis les coordonnées de chaque île. La saisie est terminée dès que l’utilisateur entre 0 trois fois.
```
Entrez la largeur : 7
Entrez la hauteur : 6
Entrez les positions et valeurs de chacune des îles, entrez 0 trois fois pour terminer
Entrez le x de l'île N° 1 : 1
Entrez le y de l'île N° 1 : 1
Entrez le nombre de ponts max de l'île N° 1 : 2
...
```

## II.2 - La sortie
Une fois l'exécution terminée, si la grille est résolue, elle est affichée en introduisant des caractères représentant les ponts créés. Si la solution n’a pas été trouvée ou s’il y a eu une erreur à l'exécution, cela sera alors affiché. Le temps d'exécution du programme en secondes est aussi affiché à la fin.

Les caractères utilisés pour représenter les ponts sont : 
```
∥  pour un double pont vertical
=  pour un double pont horizontal
|  pour un pont vertical 
-  pour un pont horizontal.
```
Ces caractères peuvent être changés dans le fichier `grid.py`
Voici un exemple de sortie pour l’exemple ci-dessus :
```
Une solution à été trouvée avec succès :
2---3=3
|1-4=4|
|  | ∥|
3--3 2|
|  |1-2
2--4=2
Exécution terminée en 0.00247955322265625s
```

# III - Notes importantes
- Une grille erronée peut quand même engendrer une longue exécution avant que cela ne soit détecté, et le programme peut même trouver une “solution”. Le comportement est donc complètement indéfini.
- Si vous ne laissez pas d’espaces entre deux îles, vous ne pourrez voir s'il y a un pont les reliant ou pas, car rien ne pourra être affiché même s'il y en a un dans la solution.
- Vous pouvez utiliser le format de sortie (avec les ponts) en tant qu’entrée, le programme ignore chaque caractère qui n’est pas numérique.
- L’entrée ne doit pas nécessairement être un carré, d’ailleurs celui montré en exemple a 6 lignes et 7 colonnes.

# IV - Grilles de test 

En utilisant le mode `F` vous pouvez renommer les fichiers `in1.txt`, `in2.txt`, `in3.txt` et `in4.txt` en `in.txt` afin de tester les grilles contenues dans ces fichiers.

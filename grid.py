from typing import Tuple

from island import Island
from bridge import Bridge

DOUBLE_V_BRIDGE = "∥"
DOUBLE_H_BRIDGE = "="
V_BRIDGE = "|"
H_BRIDGE = "-"
EMPTY = " "


class Grid:
    __slots__ = ("_width", "_height", "islands_matrix", "islands_list", "bridges_queue", "same_x", "same_y",
                 "all_possible_bridges", "connected_islands_queue")

    def __init__(self, width: int, height: int, islands: [Island], bridges: list = None):
        self._width = width
        self._height = height
        self.islands_matrix = {i: {} for i in range(width)}
        self.islands_list: [Island] = []
        self.same_y = {}
        self.same_x = {}

        # navigation helpers
        for i in islands:           # type: Island
            self.islands_matrix[i.x][i.y] = i

            self.islands_list.append(i)

            if i.x in self.same_x.keys():
                self.same_x[i.x].append(i)
            else:
                self.same_x[i.x] = [i]

            if i.y in self.same_y.keys():
                self.same_y[i.y].append(i)
            else:
                self.same_y[i.y] = [i]

        self.bridges_queue: [Bridge] = bridges or []
        self.all_possible_bridges: [Bridge] = []
        self.compute_neighbors()

        self.connected_islands_queue: [[[Island]]] = [[]]

    def __str__(self):
        m = list(list(EMPTY * self.width) for i in range(self.height))
        for y in range(self.height):
            for x in range(self.width):
                if x in self.islands_matrix.keys() and y in self.islands_matrix[x].keys():
                    m[y][x] = str(self.islands_matrix[x][y].max_val)

        for br in self.bridges_queue:   # type: Bridge
            for i in range(1, len(br)):
                if br.is_vertical:
                    m[br.y + i][br.x] = DOUBLE_V_BRIDGE if self.is_bridge_doubled(br) else V_BRIDGE
                else:
                    m[br.y][br.x + i] = DOUBLE_H_BRIDGE if self.is_bridge_doubled(br) else H_BRIDGE

        return "\n".join("".join(line) for line in m)

    @property
    def width(self):
        return self._width

    @property
    def height(self):
        return self._height

    @property
    def solved(self):
        for isl in self.islands_list:
            if isl.val != 0:
                return False
        return len(self.last_archipelagos_state) == 1 and len(self.last_archipelagos_state[0]) == len(self.islands_list)

    @property
    def last_archipelagos_state(self):
        return self.connected_islands_queue[-1]

    def is_bridge_doubled(self, bridge: Bridge):
        return self.bridges_queue.count(bridge) == 2

    def get_islands_from_bridge(self, bridge: Bridge) -> Tuple[Island, Island]:
        (x1, y1), (x2, y2) = bridge.coordinates
        return self.islands_matrix[x1][y1], self.islands_matrix[x2][y2]

    def can_build(self, bridge: Bridge):
        start, end = self.get_islands_from_bridge(bridge)
        return self.can_bridge(start, end)

    def can_bridge(self, start: Island, end: Island):
        if start.x != end.x and start.y != end.y or start.position == end.position:
            return False

        if start.val <= 0 or end.val <= 0:
            return False

        br = Bridge.from_two_islands(start, end)

        return self.bridge_has_no_intersecting_island(br) and self.no_intersecting_bridge(br)

    def bridge_has_no_intersecting_island(self, br):
        islands = self.same_x[br.x] if br.is_vertical else self.same_y[br.y]
        for isld in islands:
            if br.intersects(isld.x, isld.y):
                return False
        return True

    def no_intersecting_islands(self, start: Island, end: Island):
        br = Bridge.from_two_islands(start, end)
        return self.bridge_has_no_intersecting_island(br)

    def no_intersecting_bridge(self, br):
        for b in self.bridges_queue:
            if b == br:
                return not self.is_bridge_doubled(b)

            if br.would_intersect(b):
                return False
        return True

    def quick_can_bridge(self, start: Island, end: Island):
        return self.no_intersecting_bridge(Bridge.from_two_islands(start, end))

    def add_bridge(self, br: Bridge):

        if self.bridges_queue.count(br) == 2:
            raise Exception("Tentative de rajouter un pont qui existe déjà deux fois dans la pile")

        start, end = self.get_islands_from_bridge(br)
        start.decr()
        end.decr()

        arch_start: list = None
        arch_end: list = None
        arch_end_index = -1
        arch_start_index = -1

        for index, archipelago in enumerate(self.last_archipelagos_state):     # type: [Island]
            if start in archipelago:
                arch_start = archipelago
                arch_start_index = index
            if end in archipelago:
                arch_end = archipelago
                arch_end_index = index

        current_archipelagos_state = self.last_archipelagos_state.copy()
        if arch_start is None and arch_end is None:
            current_archipelagos_state.append([start, end])
        elif arch_start is None:
            new_arch_end = current_archipelagos_state[arch_end_index] = arch_end.copy()
            new_arch_end.append(start)
        elif arch_end is None:
            new_arch_start = current_archipelagos_state[arch_start_index] = arch_start.copy()
            new_arch_start.append(end)
        elif arch_start != arch_end:
            new_arch_start = current_archipelagos_state[arch_start_index] = arch_start.copy()
            new_arch_start.extend(arch_end)
            del current_archipelagos_state[arch_end_index]

        self.bridges_queue.append(br)
        self.connected_islands_queue.append(current_archipelagos_state)
        # print(f"Successfully added bridge {br}")
        # print(self)

# translation de g.add_bridge
# RajouterPont(g: Grille, p: Pont)
# var ileDebut: Ile, ileFin: Ile, archipelDebut: ListeIles, archipelFin: ListeIles, indexArchipelDebut: Entier,
# indexArchipelFin: Entier, etatArchipelsCourants: ListeListeIles, index: Entier, nouvelArchipel: ListeIles
#
# debut:
#
# ileDebut <- IleDebutDuPont(p)
# ileFin <- IleFinDuPont(p)
#
# ileDebut.val <- ileDebut.val - 1
# ileFin.val <- ileFin.val - 1
#
# archipelDebut <- null
# archipelFin <- null
# indexArchipelDebut <- -1
# indexArchipelFin <- -1
#
# tantque index < Taille(g.DernierElementDePileArchipels):
# 	si EstDansListe(g.DernierElementDePileArchipels[index], ileDebut):
# 		archipelDebut <- g.DernierElementDePileArchipels[index]
# 		indexArchipelDebut <- index
#
# 	si EstDansListe(g.DernierElementDePileArchipels[index], ileFin):
# 		archipelFin <- g.DernierElementDePileArchipels[index], ileFin)
# 		indexArchipelFin <- index
#
# etatArchipelsCourants <- CopieListe(g.DernierElementDePileArchipels)
# si indexArchipelDebut ==  -1 et indexArchipelFin == -1:
# 	nouvelArchipel <- NouvelleListe()
# 	AjouterListe(nouvelArchipel, debut)
# 	AjouterListe(nouvelArchipel, fin)
# 	AjouterListe(etatArchipelsCourants, nouvelArchipel)
#
# sinon si indexArchipelDebut == -1:
# 	nouvelArchipel <- CopieListe(archipelFin)
# 	etatArchipelsCourants[indexArchipelFin] <- nouvelArchipel
# 	AjouterListe(nouvelArchipel, debut)
#
# sinon si indexArchipelFin == -1:
# 	nouvelArchipel <- CopieListe(archipelDebut)
# 	etatArchipelsCourants[indexArchipelDebut] <- nouvelArchipel
# 	AjouterListe(nouvelArchipel, fin)
#
# sinon si indexArchipelDebut != indexArchipelFin:
# 	nouvelArchipel <- CopieListe(archipelDebut)
# 	etatArchipelsCourants[indexArchipelDebut] <- nouvelArchipel
# 	ConcatenerListe(nouvelArchipel, archipelFin)
# 	SupprimerElement(etatArchipelsCourants, indexArchipelFin)
#
# Empiler(g.pileDesPonts, p)
# Empiler(g.pileArchipels, etatArchipelsCourants)

    # assumes br isn't in the queue anymore
    def process_bridge_deletion(self, br: Bridge):
        start, end = self.get_islands_from_bridge(br)
        start.incr()
        end.incr()
        del br

    def cancel_last_bridge(self, br: Bridge = None):
        if br is not None:
            last = self.bridges_queue.pop()
            if br != last:
                raise Exception("Tentative d'annuler un pont qui n'est PAS le dernier!")
            self.process_bridge_deletion(last)
        else:
            self.process_bridge_deletion(self.bridges_queue.pop())
        self.connected_islands_queue.pop()

    def compute_neighbors(self):
        for isl in self.islands_list:
            for potential_neighbor in self.same_x[isl.x] + self.same_y[isl.y]:
                if potential_neighbor is not isl and self.can_bridge(isl, potential_neighbor):
                    if isl not in potential_neighbor.neighbors and potential_neighbor not in isl.neighbors:
                        isl.neighbors.append(potential_neighbor)
                        potential_neighbor.neighbors.append(isl)
                        self.all_possible_bridges.append(Bridge.from_two_islands(isl, potential_neighbor))
                        if isl.max_val >= 2 and potential_neighbor.max_val >= 2:
                            self.all_possible_bridges.append(Bridge.from_two_islands(isl, potential_neighbor))

            # print(f"{isl} has {len(isl.neighbors)} neighbors which are : {isl.neighbors}")

    def get_remaining_possible_bridges(self):
        possibilities = []
        for bridge in self.all_possible_bridges:
            if bridge not in self.bridges_queue and self.can_build(bridge):
                possibilities.append(bridge)
        return possibilities


def input_grid() -> Grid:
    width, height = int(input("Entrez la largeur : ")), int(input("Entrez la hauteur : "))
    print("Entrez les positions et valeurs de chacune des iles, entrez 0 trois fois pour terminer")
    islands = []
    while True:
        x = int(input(f"Entrez le x de l'ile N° {len(islands) + 1} : "))
        y = int(input(f"Entrez le y de l'ile N° {len(islands) + 1} : "))
        val = int(input(f"Entrez le nombre de ponts max de l'ile N° {len(islands) + 1} : "))

        if (x, y, val) == (0, 0, 0):
            break

        islands.append(Island(x - 1, y - 1, val))

    print()
    return Grid(width, height, islands)


def scan_grid(s: str) -> Grid:
    lines = s.split('\n')[1:]
    width, height = len(lines[0]), len(lines)
    islands = []
    for i in range(height):
        for j in range(width):
            try:
                max_val = int(lines[i][j])
                islands.append(Island(j, i, max_val))
            except ValueError:
                pass

    return Grid(width, height, islands)


def multi_line_input(file=None):
    s = ''
    while True:
        if file is None:
            inp = input()
        else:
            inp = file.readline().strip("\n")
        if inp.upper() == "EOF":
            break
        s = f"{s}\n{inp}"
    lines = s.split("\n")
    max_line = max(len(line) for line in lines)
    for i in range(len(lines)):
        lines[i] = f'{lines[i]:{max_line}}'
    return "\n".join(lines)


